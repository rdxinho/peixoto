class HomesController < ApplicationController


   def new
        @home = Home.new
    end

    def show
         @home = Home.find(params[:id])
    end

  def create
    @home = Home.new(home_params)

      if @home.save
         redirect_to @home, notice: 'Cadastro criado com sucesso!'
      else
         render action: :new
      end
    end

private

    def home_params
    	params.require(:home).permit(:full_name, :occupation, :description)
    end

end