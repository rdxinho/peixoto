class Home < ActiveRecord::Base

	validates_presence_of :full_name, :occupation
	validates_length_of :description, allow_blank: false

end
