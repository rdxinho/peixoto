class CreateHomes < ActiveRecord::Migration
  def change
    create_table :homes do |t|
      t.string :full_name
      t.string :occupation
      t.text :description

      t.timestamps
    end
  end
end
